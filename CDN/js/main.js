const MyFirstComponent = {
	data(){
		return {
			text: "Soy el encabezado :)",
			counter: 0,
			message: 'Fecha ' + new Date().toLocaleString(),
		}
	},
	mounted(){
		setInterval(() => {
			this.counter++
		}, 1000)
	}
}

const app = Vue.createApp(MyFirstComponent);
app.use(Quasar, {
	config: {}
});
app.mount('#app');