//numeros con solo puntos y opcionalmente con signo pesos u otro

export const handleFormattedNumber = (number, tag = null) => {
    const num = parseInt(number.split(".").join(""));
    try{
        const count = num.toString().split('');
        if (count.length > 3) {
            var dots = [], result = '';
            for (let i = count.length, j = 1; i > 0; i-- , j++) {
                if (j % 3 === 0)
                    dots.push('.' + count[i - 1]);
                else
                    dots.push(count[i - 1]);
            }

            for (let i = dots.length; i > 0; i--) {
                result += dots[i - 1];
            }

            if (result.split("").length > 0 && result.split("")[0] == ".")
                return result.split("").slice(1).join("");

            const format = result ? result.toString() : '';
            return tag && format ? tag + format : format; 
        } else{
            const format = num ? num.toString() : '';
            return tag && format ? tag + format : format;
        }
    }catch (error){
        console.log("error: ", error);
    }
}

//peticiones nativas con javascript

let body = new FormData();
body.append('name', "anyname");

xhr = new XMLHttpRequest();
xhr.upload.addEventListener("progress", e => {
    let progress = e.loaded / e.total;
    console.log("progress xhr: ", progress);
});
xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        const json = JSON.parse(xhr.responseText);
        console.log(json);
    }
}
xhr.open('POST', `${ROUTE_ENDPOINT}/v1/animals`, true);
xhr.setRequestHeader("Authorization", `Bearer ${token}`);
xhr.setRequestHeader("Content-Type", "multipart/form-data");
xhr.send(body);

//peticiones con axios

let body = new FormData();
body.append('name', "anyname");

axios.request({
    url: `${ROUTE_ENDPOINT}/v1/animals`,
    method: "POST",
    headers: {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "multipart/form-data"
    },
    data: body,
    onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
        console.log("porcentaje: ", percentCompleted);
    },
})
    .then(response => {
        const json = response.data;
            if (json.code === 422) {
                throw json.data;
            }
            if (json.code === 400) {
                DropDownHolder.alert('error', '!Error¡', `${json.data}`);
                throw json.data;
            }
            if (json.code === 200) {
                return json.data.data;
            }
            throw json.data;
        })
    .catch(error => {
        throw error;
    });